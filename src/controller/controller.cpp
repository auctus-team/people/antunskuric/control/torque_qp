#include "torque_qp/controller/controller.h"

using namespace std;
using namespace pinocchio;

namespace Controller
{
bool Controller::init(ros::NodeHandle &node_handle, Eigen::VectorXd q_init, Eigen::VectorXd qdot_init)
{
    ROS_WARN("Using torque_qp controller");

    //--------------------------------------
    // INITIALIZE PUBLISHERS
    //--------------------------------------
    initPublishers(node_handle);

    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    if (!loadRobot(node_handle))
        return false;

    //--------------------------------------
    // LOAD PARAMETERS
    //--------------------------------------
    loadParameters();

    if (!node_handle.getParam("/sim", sim))
    {
        ROS_ERROR_STREAM("Could not read parameter sim");
        return false;
    }
    //--------------------------------------
    // INITIALIZE VARIABLES
    //--------------------------------------    

    Jdot = Eigen::MatrixXd::Zero(6,dof);
    J = Eigen::MatrixXd::Zero(6,dof);
    nonLinearTerms.resize(dof);

    //--------------------------------------
    // QPOASES
    //--------------------------------------
    qp = qp_solver.configureQP(number_of_variables, number_of_constraints);
    joint_torque_out.resize(number_of_variables);

    //--------------------------------------
    // INITIALIZE RBOT STATE
    //--------------------------------------
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::framesForwardKinematics(*model,*data,q_init);
    Eigen::Affine3d oMtip;
    oMtip = data->oMf[model->getFrameId(tip_link)].toHomogeneousMatrix();

    //--------------------------------------
    // BUILD TRAJECTORY
    //--------------------------------------
    std::string panda_traj_path = ros::package::getPath("panda_traj");
    std::string trajectoryfile = panda_traj_path+"/trajectories/go_to_point.csv";
    std::string csv_file_name = trajectoryfile;
    trajectory.Load(csv_file_name);
    trajectory.Build(oMtip, true);
   
    ROS_DEBUG_STREAM(" Trajectory computed ");
    return true;
}

Eigen::VectorXd Controller::update(Eigen::VectorXd q, Eigen::VectorXd qdot, const ros::Duration &period)
{
    double time_dt = period.toSec();
    
    // Update the model
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::framesForwardKinematics(*model,*data,q);
    pinocchio::computeMinverse(*model,*data,q);
    data->Minv.triangularView<Eigen::StrictlyLower>() = data->Minv.transpose().triangularView<Eigen::StrictlyLower>();
    pinocchio::computeGeneralizedGravity(*model,*data,q);
    pinocchio::computeCoriolisMatrix(*model,*data,q,qdot);
    pinocchio::computeJointJacobians(*model,*data,q);
    pinocchio::getFrameJacobian(*model, *data, model->getFrameId(tip_link), pinocchio::ReferenceFrame::LOCAL, J);
    pinocchio::getFrameJacobianTimeVariation(*model,*data,model->getFrameId(tip_link),pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED,Jdot);
    pinocchio::Motion xd = getFrameVelocity(*model, *data, model->getFrameId(tip_link));

    //Update the trajectory
    trajectory.updateTrajectory(traj_properties, time_dt);
    pinocchio::SE3 oMdes(trajectory.Pos().matrix());
    
    // Compute error
    const pinocchio::SE3 tipMdes = data->oMf[model->getFrameId(tip_link)].actInv(oMdes);
    err = pinocchio::log6(tipMdes).toVector();
   
    xdd_des = trajectory.Acc() + p_gains.cwiseProduct(err) - d_gains.cwiseProduct(xd.toVector());

    // Formulate QP problem such that
    // joint_torque_out = argmin 1/2 tau^T H tau + tau^T g_
    //                       s.t     lbA < A tau < ubA
    //                                   lb < tau < ub 
    nonLinearTerms = data->Minv * (data->C + data->g);
    
    qp.hessian = 2.0 * regularisation_weight *  data->Minv;
    qp.gradient = -2.0 * regularisation_weight *  data->Minv * (data->g - k_reg.asDiagonal()  * qdot);
    
    a.noalias() = J * data->Minv;
    b.noalias() = -J *  nonLinearTerms +  Jdot * qdot - xdd_des;

    qp.hessian += 2.0 * a.transpose() *  a;
    qp.gradient += 2.0 * a.transpose() * b;

    qp.ub =  model->effortLimit;
    qp.lb = -model->effortLimit;
    
    double horizon_dt = 15 * time_dt;
    qp.a_constraints.block(0, 0, dof, dof) = data->Minv;
    qp.lb_a.block(0, 0, dof, 1) =
        ((-model->velocityLimit - qdot) / horizon_dt + nonLinearTerms)
            .cwiseMax(2 * (model->lowerPositionLimit - q - qdot * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms);

    qp.ub_a.block(0, 0, dof, 1) =
        ((model->velocityLimit - qdot) / horizon_dt + nonLinearTerms)
            .cwiseMin(2 * (model->upperPositionLimit - q - qdot * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms);
    
    joint_torque_out = qp_solver.SolveQP(qp);
    
    // Publish some messages
    doPublishing();

    // Remove gravity if on the real robot as Franka does it for us
    if (!sim)
        joint_torque_out -= data->g;

    return joint_torque_out;
}

void Controller::buildTrajectory(Eigen::Affine3d X_curr)
{
    trajectory.Build(X_curr, false);
    publishTrajectory();
}

void Controller::publishTrajectory()
{
    panda_traj::PublishTraj publish_traj_;
    publish_traj_ = trajectory.publishTrajectory();

    nav_msgs::Path path_ros;
    path_ros.poses = publish_traj_.path_ros_.poses;
    path_ros.header.frame_id = "world";
    path_ros.header.stamp = ros::Time::now();
    geometry_msgs::PoseArray pose_array;
    pose_array.poses = publish_traj_.pose_array_.poses;
    pose_array.header.frame_id = "world";
    pose_array.header.stamp = ros::Time::now();

    if (pose_array_publisher.trylock())
    {
        pose_array_publisher.msg_.header.stamp = ros::Time::now();
        pose_array_publisher.msg_.header.frame_id = "world";
        pose_array_publisher.msg_ = pose_array;
        pose_array_publisher.unlockAndPublish();
    }
    if (path_publisher.trylock())
    {
        path_publisher.msg_.header.stamp = ros::Time::now();
        path_publisher.msg_.header.frame_id = "world";
        path_publisher.msg_ = path_ros;
        path_publisher.unlockAndPublish();
    }
    ROS_INFO_STREAM(" Trajectory published ");
}

void Controller::initPublishers(ros::NodeHandle &node_handle)
{
    // Realtime safe publishers
    pose_array_publisher.init(node_handle, "Pose_array", 1);
    path_publisher.init(node_handle, "Ros_Path", 1);
    pose_curr_publisher.init(node_handle, "X_curr", 1);
    pose_des_publisher.init(node_handle, "X_traj", 1);
    panda_rundata_publisher.init(node_handle, "panda_rundata", 1);
}

void Controller::loadParameters()
{
    ROS_INFO_STREAM("------------- Loading parameters -------------");
    p_gains.resize(6);
    getRosParam("/torque_qp/p_gains", p_gains,true);
    d_gains.resize(6);
    getRosParam("/torque_qp/d_gains", d_gains,true);
    k_reg.resize(dof);
    getRosParam("/torque_qp/k_reg", k_reg,true);
    getRosParam("/torque_qp/regularisation_weight", regularisation_weight,true);
    getRosParam("/torque_qp/tip_link",tip_link,true);
    ROS_INFO_STREAM("------------- Parameters Loaded -------------");
}

bool Controller::loadRobot(ros::NodeHandle& node_handle)
{
    updateUI_service = node_handle.advertiseService("updateUI", &Controller::updateUI, this);
    updateTraj_service = node_handle.advertiseService("updateTrajectory", &Controller::updateTrajectory, this); 

    // get robot descritpion
    std::string urdf_param;
    getRosParam("/robot_description", urdf_param);
    
    // Load the urdf model
    model.reset(new pinocchio::Model);
    pinocchio::urdf::buildModelFromXML(urdf_param,*model,false);
    data.reset(new pinocchio::Data(*model));

    dof = model->nv;

    number_of_variables = dof;
    number_of_constraints = dof;
    
    return true;
}

void Controller::doPublishing()
{
    // Publishing
    Eigen::Affine3d xcurr,xdes;
    xcurr =  data->oMf[model->getFrameId(tip_link)].toHomogeneousMatrix();
    tf::poseEigenToMsg(xcurr,X_curr_msg);
    
    pinocchio::SE3 oMdes(trajectory.Pos().matrix());
    xdes=oMdes.toHomogeneousMatrix();

    tf::poseEigenToMsg(xdes,X_traj_msg);
    tf::twistEigenToMsg(err, X_err_msg);
    const ros::Time rosnow = ros::Time::now();

    if (panda_rundata_publisher.trylock())
    {
        panda_rundata_publisher.msg_.header.stamp = rosnow;
        panda_rundata_publisher.msg_.header.frame_id = "world";
        panda_rundata_publisher.msg_.X_err = X_err_msg;
        panda_rundata_publisher.msg_.play_traj_ = traj_properties.play_traj_;
        panda_rundata_publisher.msg_.tune_gains_ = traj_properties.gain_tunning_ ;
        panda_rundata_publisher.unlockAndPublish();
    }

  if (pose_curr_publisher.trylock())
  {
    pose_curr_publisher.msg_.header.stamp = rosnow;
    pose_curr_publisher.msg_.header.frame_id = "world";
    pose_curr_publisher.msg_.pose = X_curr_msg;
    pose_curr_publisher.unlockAndPublish();
  }

  if (pose_des_publisher.trylock())
  {
    pose_des_publisher.msg_.header.stamp = rosnow;
    pose_des_publisher.msg_.header.frame_id = "world";
    pose_des_publisher.msg_.pose = X_traj_msg;
    pose_des_publisher.unlockAndPublish();
  }
}

// Ros service to interact with the code
bool Controller::updateUI(torque_qp::UI::Request &req, torque_qp::UI::Response &resp)
{
    traj_properties.play_traj_ = req.play_traj;
    if (req.publish_traj)
        publishTrajectory();
    if (req.build_traj)
    {
        Eigen::Affine3d X_curr(data->oMf[model->getFrameId(tip_link)].toHomogeneousMatrix()); 
        buildTrajectory(X_curr);
    }

    if (req.exit_)
    {
        ros::shutdown();
        exit(0);
    } 
    resp.result = true;

    return true;
}

// Ros service to update the trajectory
bool Controller::updateTrajectory(panda_traj::UpdateTrajectory::Request& req, panda_traj::UpdateTrajectory::Response& resp){

  trajectory.Load(req.csv_traj_path);
  Eigen::Affine3d X_curr(data->oMf[model->getFrameId(tip_link)].toHomogeneousMatrix()); 
  trajectory.Build(X_curr, req.verbose);
  publishTrajectory();
  std::cout << "Received waypoint computing traj and publishing" << std::endl;

  return true;
}

} // namespace Controller
